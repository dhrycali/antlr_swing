package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	private LocalSymbols mem = new LocalSymbols();
	private GlobalSymbols globMem = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer mod(Integer e1, Integer e2) {
		if (e2 == 0) {
			throw new RuntimeException("Error: Mod przez zero");
		}
		return e1%e2;
	}
	
	protected Integer div(Integer e1, Integer e2) {
		if (e2 == 0) {
			throw new RuntimeException("Error: Dzielenie przez zero");
		}
		return (int) e1/e2;
	}
	
	protected Integer pow(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}
	
	protected Integer log(Integer e1, Integer e2) {
	    return (int) (Math.log(e2) / Math.log(e1));
	}
	
	protected void declVar(String id) {
		if(mem.hasSymbol(id)) {
			throw new RuntimeException("Zmienna zdeklarowana: "+id);
		}
		mem.newSymbol(id);
	}
	
	protected Integer setVar(String id, Integer value) {
		if(mem.hasSymbol(id)) {
			return mem.setSymbol(id, value);			
		}
		globMem.setSymbol(id, value);
		return value;
	}
	
	protected Integer getVar(String id) {
		if(!mem.hasSymbol(id) && !globMem.hasSymbol(id)) {
			throw new RuntimeException("Zmienna nie jest zdeklarowana: "+id);
		} else if(mem.hasSymbol(id)) {
			return mem.getSymbol(id);
		}
		return globMem.getSymbol(id);
	}
	
	protected Integer enterScope() {
		return mem.enterScope();
	}
	
	protected Integer leaveScope() {
		return mem.leaveScope();
	}
	
	protected void declGlobVar(String id) {
		if(globMem.hasSymbol(id)) {
			throw new RuntimeException("Zmienna zdeklarowana: "+id);
		}
		globMem.newSymbol(id);
	}
}
