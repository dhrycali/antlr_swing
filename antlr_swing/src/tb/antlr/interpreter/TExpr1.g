tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr
        | print
        | globVar
        | blockState)*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = div($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = mod($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = pow($e1.out, $e2.out);}
        | ^(LOG   e1=expr e2=expr) {$out = log($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setVar($i1.text, $e2.out);}
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
        
print   : ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        ;

var     : ^(VAR id=ID) {declVar($id.text);}
        ;

globVar : ^(VAR id=ID) {declGlobVar($id.text);}
        ;

blockState
        : (LB {enterScope();}
        ( expr
        | print
        | var
        | blockState)*
        RB {leaveScope();})
        ;